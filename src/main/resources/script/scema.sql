CREATE TABLE invoice_detail_tb (
    id serial primary key ,
    product_id int4 REFERENCES  product_tb(id),
    invoice_id int4 REFERENCES invoice_tb(id)
)
SELECT c.id, c.product_name, c.product_price FROM product_tb bc INNER JOIN invoice_tb c ON c.id = bc.id WHERE bc.id =2