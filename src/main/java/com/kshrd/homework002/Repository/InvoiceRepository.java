package com.kshrd.homework002.Repository;

import com.kshrd.homework002.model.entity.Invoice;
import com.kshrd.homework002.model.request.InvoiceRequest;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface InvoiceRepository {
//    @Select("INSERT INTO invoice_tb (invoice_date) ")
//    Invoice AddNewInvoice(InvoiceRequest invoiceRequest);
    @Select("SELECT * FROM invoice_tb")
//    @Result(property = "invoiceId")
    @Result(property = "customer",column = "id",
    one = @One(select = "com.kshrd.homework002.Repository.CustomerRepositoy.getCustmerById")
    )
    @Result(property = "products",column = "id" ,
            many = @Many(select = "com.kshrd.homework002.Repository.ProductRepositery.getProductbyId"))
    List<Invoice> findAllInvoice();
}
