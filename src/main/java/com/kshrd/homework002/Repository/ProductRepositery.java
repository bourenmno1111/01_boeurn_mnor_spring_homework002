package com.kshrd.homework002.Repository;

import com.kshrd.homework002.model.entity.Product;
import com.kshrd.homework002.model.request.ProductRequest;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import java.util.List;

@Mapper
public interface ProductRepositery {
    @Select("SELECT * FROM product_tb")
    List<Product> fineAllProduct();
    @Select("SELECT * FROM product_tb WHERE id = #{productId}")
    Product getProductbyId(Integer productId);
    @Delete("DELETE FROM product_tb WHERE id = #{productId}")
    boolean deleteProductById(Integer productId);
    @Select("INSERT INTO product_tb (product_name, product_price) VALUES(#{request.product_name},#{request.product_price}) " +
    "RETURNING id")
    Integer saveProduct(@Param("request") ProductRequest productRequest);
    @Select("UPDATE product_tb SET product_name = #{request.product_name}, " +
            " product_price = #{request.product_price} " +
            " WHERE id = #{productId}"+
    "RETURNING id")
    Integer updateProduct(@Param("request") ProductRequest productRequest, Integer productId);
}
