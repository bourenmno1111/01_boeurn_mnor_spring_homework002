package com.kshrd.homework002.Repository;

import com.kshrd.homework002.model.entity.Product;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface InvoiceDetailRepository {
    @Select("SELECT c.id, c.product_name, c.product_price FROM product_tb c " +
            "INNER JOIN invoice_tb bc ON c.id = bc.id" +
            "WHERE bc.id = #{productId} ")
    List<InvoiceDetailRepository> getProductById();
}
