package com.kshrd.homework002.Repository;

import com.kshrd.homework002.model.entity.Customer;
import com.kshrd.homework002.model.request.CustomerRequest;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface CustomerRepositoy {
    @Select("SELECT * FROM customer_tb")
    List<Customer> findAllCustomer();
    @Select("SELECT * FROM customer_tb WHERE id =#{customerId}")
    Customer getCustmerById(Integer customerId);
    @Select("INSERT INTO customer_tb (customer_name, customer_address,customer_phone)" +
            "VALUES ( #{customer_name} , #{customer_address}, #{customer_phone}) " +
            "RETURNING id")
    Integer addNewCustomer(CustomerRequest customerRequest);
    @Delete("DELETE FROM customer_tb WHERE id = #{customerId}")
        boolean deletCustomerById(Integer customerId);
    @Select("UPDATE customer_tb SET customer_name = #{request.customer_name}," +
            "customer_address = #{request.customer_address}," +
            "customer_phone = #{request.customer_phone}" +
            "WHERE id = #{customerId} " + "RETURNING id")
    Integer udpdateCustomerById(@Param("request") CustomerRequest customerRequest,Integer customerId);

}
