package com.kshrd.homework002.service;

import com.kshrd.homework002.model.entity.Product;
import com.kshrd.homework002.model.request.ProductRequest;

import java.util.List;

public interface ProductService {
     List<Product> getProduct();
     Product getProductById(Integer productId);
     boolean deleteProductById(Integer productId);
     Integer addnewProduct(ProductRequest productRequese);
     Integer updateProduct(ProductRequest productRequest,Integer productId);


}
