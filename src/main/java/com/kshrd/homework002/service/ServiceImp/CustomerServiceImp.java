package com.kshrd.homework002.service.ServiceImp;

import com.kshrd.homework002.Repository.CustomerRepositoy;
import com.kshrd.homework002.model.entity.Customer;
import com.kshrd.homework002.model.request.CustomerRequest;
import com.kshrd.homework002.service.CustomerService;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class CustomerServiceImp implements CustomerService {
 public final CustomerRepositoy customerRepositoy;

    public CustomerServiceImp(CustomerRepositoy customerRepositoy) {
        this.customerRepositoy = customerRepositoy;
    }

    @Override
    public List<Customer> getCustomer() {
        return customerRepositoy.findAllCustomer();
    }

    @Override
    public Customer getcustomerById(Integer customerId) {
        return customerRepositoy.getCustmerById(customerId);
    }

    @Override
    public Integer addNewCustomer(CustomerRequest customerRequest) {
        return customerRepositoy.addNewCustomer(customerRequest);
    }

    @Override
    public boolean deleteCutomer(Integer id) {
        return customerRepositoy.deletCustomerById(id);
    }

    @Override
    public Integer updateCustomerById(CustomerRequest customerRequest, Integer customerId) {
        return customerRepositoy.udpdateCustomerById(customerRequest,customerId);
    }
}
