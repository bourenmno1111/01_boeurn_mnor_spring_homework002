package com.kshrd.homework002.service.ServiceImp;

import com.kshrd.homework002.Repository.ProductRepositery;
import com.kshrd.homework002.model.entity.Product;
import com.kshrd.homework002.model.request.ProductRequest;
import com.kshrd.homework002.service.ProductService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductServiceImp implements ProductService {
    private final ProductRepositery productRepositery;
    public ProductServiceImp(ProductRepositery productRepositery){
        this.productRepositery = productRepositery;
    }
    @Override
    public List<Product> getProduct() {
        return productRepositery.fineAllProduct();
    }

    @Override
    public Product getProductById(Integer productId) {
        return productRepositery.getProductbyId(productId);
    }

    @Override
    public boolean deleteProductById(Integer productId) {
        return productRepositery.deleteProductById(productId);
    }

    @Override
    public Integer addnewProduct(ProductRequest productRequese) {
        Integer productId =productRepositery.saveProduct(productRequese);
        return  productId ;
    }

    @Override
    public Integer updateProduct(ProductRequest productRequest, Integer productId) {
        Integer productIdUpdate = productRepositery.updateProduct(productRequest , productId);
        return productIdUpdate;
    }


}
