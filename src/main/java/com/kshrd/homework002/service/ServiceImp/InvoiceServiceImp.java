package com.kshrd.homework002.service.ServiceImp;

import com.kshrd.homework002.Repository.InvoiceRepository;
import com.kshrd.homework002.model.entity.Invoice;
import com.kshrd.homework002.model.request.InvoiceRequest;
import com.kshrd.homework002.service.InvoiceService;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class InvoiceServiceImp implements InvoiceService {
 private  final InvoiceRepository invoiceRepository;

    public InvoiceServiceImp(InvoiceRepository invoiceRepository) {
        this.invoiceRepository = invoiceRepository;
    }

//    @Override
//    public Invoice addNewInvoice(InvoiceRequest invoiceRequest) {
//        return null;
//    }

    @Override
    public List<Invoice> getAllinvoice() {
        return invoiceRepository.findAllInvoice();
    }
}
