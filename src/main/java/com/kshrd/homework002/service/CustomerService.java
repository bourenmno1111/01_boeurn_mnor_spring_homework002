package com.kshrd.homework002.service;

import com.kshrd.homework002.model.entity.Customer;
import com.kshrd.homework002.model.request.CustomerRequest;

import java.util.List;

public interface CustomerService {
    List<Customer> getCustomer();
    Customer getcustomerById(Integer id);
    Integer addNewCustomer(CustomerRequest customerRequest);
    boolean deleteCutomer(Integer id);
    Integer updateCustomerById(CustomerRequest customerRequest, Integer customerId);


}
