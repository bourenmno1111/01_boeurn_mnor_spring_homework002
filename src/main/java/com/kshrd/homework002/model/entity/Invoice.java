package com.kshrd.homework002.model.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Invoice {
    private Integer id;
    private Timestamp invoice_date;
//    private Integer customer_id;
    private Customer customer;
    private  List<Product> products;
}

