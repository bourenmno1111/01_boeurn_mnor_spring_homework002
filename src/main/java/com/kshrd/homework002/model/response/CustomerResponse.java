package com.kshrd.homework002.model.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;
@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class CustomerResponse<T> {
    private  String message;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private T payload;
    private Boolean succcess;
    private Timestamp timestamp;
}
