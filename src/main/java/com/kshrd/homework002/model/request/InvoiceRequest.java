package com.kshrd.homework002.model.request;

import com.kshrd.homework002.model.entity.Customer;
import com.kshrd.homework002.model.entity.Product;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class InvoiceRequest {
    private String invoice_date;
    private Customer customer;
    private List<Product> products;
}
