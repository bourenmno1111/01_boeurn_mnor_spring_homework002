package com.kshrd.homework002.controller;
import com.kshrd.homework002.model.entity.Product;
import com.kshrd.homework002.model.request.ProductRequest;
import com.kshrd.homework002.model.response.ProductResponse;
import com.kshrd.homework002.service.ProductService;
import io.swagger.v3.oas.annotations.Operation;
import org.apache.ibatis.annotations.Delete;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.List;

@RestController
@RequestMapping("/api/v1/products")
public class ProductController {
    public final ProductService productService;

    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping("/all")
    @Operation(summary ="Get all product")
    public ResponseEntity<ProductResponse<List<Product>>> getAllProduct(){
            ProductResponse<List<Product>> response = ProductResponse.<List<Product>>builder()
                    .message("Successfull")
                    .payload(productService.getProduct())
                    .succcess(true)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();


        return ResponseEntity.ok(response);
    }
    @GetMapping("/{id}")
    @Operation(summary = "Get Product by id")
    public ResponseEntity<ProductResponse<Product>> getProductbyId(@PathVariable("id") Integer productId){
        ProductResponse<Product> response = ProductResponse.<Product>builder()
                .message("Successfull")
                .succcess(true)
                .payload(productService.getProductById(productId))
                .timestamp(new Timestamp(System.currentTimeMillis()))
                .build();
        return ResponseEntity.ok(response);
    }
    @DeleteMapping("/{id}")
    @Operation(summary = "Delete Product by Id")
    public ResponseEntity<ProductResponse<Product>> deleteProductById(@PathVariable("id") Integer productId){
        ProductResponse<Product> response = null;
        if(productService.deleteProductById(productId) == true){
            response = ProductResponse.<Product>builder()
                    .message("Delect Successfull")
                    .succcess(true)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
        }

        return ResponseEntity.ok(response);
    }
    @PostMapping
    @Operation(summary = "Add new Product")
    public ResponseEntity<ProductResponse<Product>> addNewProduct(@RequestBody ProductRequest productRequest){
        Integer storProductId = productService.addnewProduct(productRequest);
        ProductResponse<Product> response =null;
        if(storProductId != null){
            response= ProductResponse.<Product>builder()
                    .message("Success")
                    .payload(productService.getProductById(storProductId))
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return ResponseEntity.ok(response);
        }
        return null;
    }
    @PutMapping("/{id}")
    @Operation(summary =  "Update by Id")
    public ResponseEntity<ProductResponse<Product>> updateProductById(
         @RequestBody ProductRequest productRequest, @PathVariable("id") Integer productid){
        ProductResponse<Product> response = null;
        Integer productUpdateById = productService.updateProduct(productRequest, productid);
        if(productUpdateById != null){
            response= ProductResponse.<Product>builder()
                    .message("Update Successfully")
                    .payload(productService.getProductById(productUpdateById))
                    .succcess(true)
                    .build();
        }
        return ResponseEntity.ok(response);
    }
}