package com.kshrd.homework002.controller;

import com.kshrd.homework002.model.entity.Customer;
import com.kshrd.homework002.model.entity.Product;
import com.kshrd.homework002.model.request.CustomerRequest;
import com.kshrd.homework002.model.response.CustomerResponse;
import com.kshrd.homework002.service.CustomerService;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.List;

@RestController
@RequestMapping("/api/v1/customer")
public class CustomerController {
    private final CustomerService customerService;

    public CustomerController(CustomerService customerService) {
        this.customerService = customerService;
    }


    @GetMapping("/all")
    @Operation(summary = "Get all Customer")
    public ResponseEntity<CustomerResponse<List<Customer>>> getAllCustomer(){
        CustomerResponse<List<Customer>> response = CustomerResponse.<List<Customer>>builder()
        .message("Successfull")
        .payload(customerService.getCustomer())
        .succcess(true)
        .timestamp(new Timestamp(System.currentTimeMillis()))
        .build();
        return ResponseEntity.ok(response);
    }
    @GetMapping("/{id}")
    @Operation(summary = "Get Customer by Id")
    public ResponseEntity<CustomerResponse<Customer>> getCustomerById(@PathVariable("id") Integer customerId){
        CustomerResponse<Customer> response = CustomerResponse.<Customer>builder()
                .message("Success")
                .payload(customerService.getcustomerById(customerId))
                .succcess(true)
                .timestamp(new Timestamp(System.currentTimeMillis()))
                .build();

        return ResponseEntity.ok(response);
    }
    @PostMapping
    @Operation(summary = "Add new customer")
    public ResponseEntity<CustomerResponse<Customer>> addNewCustomer(@RequestBody CustomerRequest customerRequest) {
        Integer storeId = customerService.addNewCustomer(customerRequest);
        CustomerResponse<Customer> response = null;
        System.out.println(customerService.getcustomerById(storeId));
        if (storeId != null) {
            response = CustomerResponse.<Customer>builder()
                    .message("Add new Customer Successfully")
                    .payload(customerService.getcustomerById(storeId))
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .succcess(true)
                    .build();
            return ResponseEntity.ok(response);
        }

        return null;
    }
    @DeleteMapping("/{id}")
    @Operation(summary = "Delete Customer by id")
    public ResponseEntity<CustomerResponse<Customer>> deleteCustomerById(@PathVariable Integer id)
    {
        CustomerResponse<Customer> response =null;
        if (customerService.deleteCutomer(id) ==true){
            response = CustomerResponse.<Customer>builder()
                    .message("Delete successfully")
                    .succcess(true)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return ResponseEntity.ok(response);
        }
        return null;
    }
    @PutMapping("/{id}")
    @Operation(summary = "update customer by id")
    public ResponseEntity<CustomerResponse<Customer>> updateCustomerById(@RequestBody CustomerRequest customerRequest, @PathVariable("id") Integer customerId){
CustomerResponse<Customer> response= null;
Integer updateCutomerById = customerService.updateCustomerById(customerRequest, customerId);
    if(updateCutomerById !=null){
        response = CustomerResponse.<Customer>builder()
                .message("Update sussessfully ")
                .payload(customerService.getcustomerById(updateCutomerById))
                .succcess(true)
                .timestamp(new Timestamp(System.currentTimeMillis()))
                .build();
        return ResponseEntity.ok(response);
    }
    return null;

    }


}




