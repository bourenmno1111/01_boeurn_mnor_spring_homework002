package com.kshrd.homework002.controller;

import com.kshrd.homework002.model.entity.Invoice;
import com.kshrd.homework002.service.InvoiceService;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/vi/invoice")
public class InvoiveController {
    private final InvoiceService invoiceService;

    public InvoiveController(InvoiceService invoiceService) {
        this.invoiceService = invoiceService;
    }


    @GetMapping("/all")
    @Operation(summary = "get all invoice")
    public ResponseEntity<List<Invoice>> getAllInvoice(){
        return  ResponseEntity.ok(invoiceService.getAllinvoice());
    }


}
